package com.jimenez.allresumen.ElemAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jimenez.allresumen.R;

import java.util.List;

public class AdaptadorRes extends RecyclerView.Adapter <CajaElemenLis> {

    List<String> lisElementos;

    public AdaptadorRes (List<String>dato) {lisElementos=dato;}

    @NonNull
    @Override
    public CajaElemenLis onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View cajalayout;
        cajalayout= LayoutInflater.from(parent.getContext()).inflate(R.layout.caja_elemen_lis,parent,false);
        return new CajaElemenLis(cajalayout);
    }

    @Override
    public void onBindViewHolder(@NonNull CajaElemenLis holder, int position) {

        String misdatos= lisElementos.get(position);
        holder.txtElemenLis.setText(misdatos);

    }

    @Override
    public int getItemCount() {
        if (lisElementos!=null){
            return lisElementos.size();
        }else {
            return 0;
        }
    }
}
