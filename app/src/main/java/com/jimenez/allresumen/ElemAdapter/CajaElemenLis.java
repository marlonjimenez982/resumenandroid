package com.jimenez.allresumen.ElemAdapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jimenez.allresumen.R;

public class CajaElemenLis extends RecyclerView.ViewHolder {

    TextView txtElemenLis;

    public CajaElemenLis(@NonNull View itemView) {
        super(itemView);

        txtElemenLis= itemView.findViewById(R.id.txtElemenLis);

    }
}
