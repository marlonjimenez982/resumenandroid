package com.jimenez.allresumen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.mariuszgromada.math.mxparser.Expression;

public class ActivityCal extends AppCompatActivity implements View.OnClickListener{

    //1-Crear
    MaterialButton botonC, botonParenAbi, botonParenCerra, botonMultiplicar,
            boton7, boton8, boton9, botonDividir,
            boton4, boton5, boton6, botonSumar,
            boton1, boton2, boton3, botonRestar,
            botonAC, boton0, botonPunto, botonIgual, btnAtras;
    TextView expresion, resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_calculadora);

        botonC=findViewById(R.id.boton_C);
        botonAC=findViewById(R.id.boton_AC);
        botonParenAbi=findViewById(R.id.boton_parenAbi);
        botonParenCerra=findViewById(R.id.boton_parencerra);
        botonMultiplicar=findViewById(R.id.boton_multiplicacion);
        botonDividir=findViewById(R.id.boton_dividir);
        botonSumar=findViewById(R.id.boton_sumar);
        botonRestar=findViewById(R.id.boton_restar);
        botonIgual=findViewById(R.id.boton_igual);
        botonPunto=findViewById(R.id.boton_punto);
        boton1=findViewById(R.id.boton_Uno);
        boton2=findViewById(R.id.boton_Dos);
        boton3=findViewById(R.id.boton_Tres);
        boton4=findViewById(R.id.boton_Cuatro);
        boton5=findViewById(R.id.boton_Cinco);
        boton6=findViewById(R.id.boton_Seis);
        boton7=findViewById(R.id.boton_Siete);
        boton8=findViewById(R.id.boton_Ocho);
        boton9=findViewById(R.id.boton_Nueve);
        boton0=findViewById(R.id.boton_Cero);
        expresion=findViewById(R.id.expresion);
        resultado=findViewById(R.id.resultado);
        btnAtras=findViewById(R.id.btnAtras);

        botonParenAbi.setOnClickListener(this);
        botonParenCerra.setOnClickListener(this);
        botonPunto.setOnClickListener(this);
        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);
        botonC.setOnClickListener(this);
        botonAC.setOnClickListener(this);
        botonIgual.setOnClickListener(this);
        botonRestar.setOnClickListener(this);
        botonSumar.setOnClickListener(this);
        botonDividir.setOnClickListener(this);
        botonMultiplicar.setOnClickListener(this);
        btnAtras.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        MaterialButton botonCal=(MaterialButton) view;
        String textoCal;
        textoCal= botonCal.getText().toString();

        String cadena;
        cadena= expresion.getText().toString();

        if (textoCal.equals("ATRAS")){
            Intent Resc = new Intent(this, ActivityRes.class);
            startActivity(Resc);
            expresion.setText("0");
            resultado.setText("0");
            return;
        }

        if (textoCal.equals("C")){
            expresion.setText("0");
            return;
        }
        if (textoCal.equals("AC")){
            expresion.setText("0");
            resultado.setText("0");
            return;
        }

        if (textoCal.equals(".")){
        }else if (cadena.equals("0")){
            cadena="";
        }

        if (textoCal.equals("=")){
            Expression analizador= new Expression(cadena);
            Double resulta= analizador.calculate();
            if (Double.isNaN(resulta)){
                resultado.setText("Corregir cálculo!");
            }else{
                resultado.setText(Double.toString(resulta));
            }
        }else {
            //concatenar lo digitado
            cadena = cadena + textoCal;
            expresion.setText(cadena);
            return;
        }

    }

}