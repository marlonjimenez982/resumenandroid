package com.jimenez.allresumen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.jimenez.allresumen.ElemAdapter.AdaptadorRes;

import java.util.ArrayList;
import java.util.List;

public class ActivityRes extends AppCompatActivity implements View.OnClickListener{

    RecyclerView rvLista;
    AdaptadorRes miAdaptador;

    //Botones
    MaterialButton btnmapa, btncalculadora, btnlistaEquipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiy_res);

        //Botones
        btnmapa=findViewById(R.id.btnmapa);
        btncalculadora=findViewById(R.id.btncalculadora);
        btnlistaEquipos=findViewById(R.id.btnlistaEquipos);

        btnmapa.setOnClickListener(this);
        btncalculadora.setOnClickListener(this);
        btnlistaEquipos.setOnClickListener(this);

        //RecyclerView
        rvLista = findViewById(R.id.rvLista);
        rvLista.setLayoutManager(new LinearLayoutManager(this));

        List<String> datosLocales =new ArrayList<>();

        datosLocales.add("¡BIENVENIDO!");
        datosLocales.add("De las siguientes aplicaciones");
        datosLocales.add("seleccione la de que desee");
        datosLocales.add("pulsando el 'Botón' que requiera.");

        miAdaptador = new AdaptadorRes(datosLocales);
        rvLista.setAdapter(miAdaptador);
    }

    @Override
    public void onClick(View view) {

        MaterialButton botonRes=(MaterialButton) view;
        String textoRes;
        textoRes= botonRes.getText().toString();
        if (textoRes.equals("Mapa")){

            Intent mapas=new Intent(this, ActivityMapa.class);
            startActivity(mapas);

        } else if (textoRes.equals("Calculadora")){

            Intent cal=new Intent(this, ActivityCal.class);
            startActivity(cal);

        }else if (textoRes.equals("Equipos Colombianos")){

            Intent equip=new Intent(this, ActivityListaEquipos.class);
            startActivity(equip);

        }

    }
}