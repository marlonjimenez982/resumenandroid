package com.jimenez.allresumen;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.google.android.material.button.MaterialButton;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

public class ActivityMapa extends AppCompatActivity implements View.OnClickListener {

    MapView mapa;
    IMapController mapControlador;
    MyLocationNewOverlay miubicacion;

    MaterialButton btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        //Boton Atras
        btnAtras=findViewById(R.id.btnAtras);
        btnAtras.setOnClickListener(this);

        //Mapa
        mapa=(MapView) findViewById(R.id.mapa);
        Context ctx = getApplicationContext();

        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.setMultiTouchControls(true);//usar del touch

        //pedir permisos sino funciona
        String[] permisos= new String[]{

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            requestPermissions(permisos,1);
        }

        //COLOCAR MARCADOR DEL GPS
        mapControlador=mapa.getController();
        miubicacion= new MyLocationNewOverlay(new GpsMyLocationProvider(this),mapa);
        //habilitar la actualización al moverse
        miubicacion.enableMyLocation();

        //agregar señal al mapa
        mapa.getOverlays().add(miubicacion);

        miubicacion.runOnFirstFix(
                ()->{
                    runOnUiThread(
                            ()->{
                                mapControlador.setZoom((double)15);
                                mapControlador.setCenter(miubicacion.getMyLocation());
                                Log.d(TAG, "Ubicacion!!" + miubicacion.getMyLocation().getLatitude()+miubicacion.getMyLocation().getLongitude());

                            });
                }
        );

    }

    @Override
    public void onClick(View view) {
        MaterialButton botonMap=(MaterialButton) view;
        String textoMap;
        textoMap= botonMap.getText().toString();

        if (textoMap.equals("ATRAS")){
            Intent Resc = new Intent(this, ActivityRes.class);
            startActivity(Resc);
        }


    }
}