package com.jimenez.allresumen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.jimenez.allresumen.Equipos.Escudos;


public class ActivityListaEquipos extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    ListView equipos;
    MaterialButton btnAtraslis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_equipos);

        //Boton Atras
        btnAtraslis=findViewById(R.id.btnAtraslis);
        btnAtraslis.setOnClickListener(this);

        equipos=findViewById(R.id.listadoEquipos);

        String[] misequipos;

        misequipos = getResources().getStringArray(R.array.equipos);

        ArrayAdapter cargadorEquipos;
        cargadorEquipos=new ArrayAdapter(this,R.layout.equipo,misequipos);

        equipos.setAdapter(cargadorEquipos);

        //Definir el adaptador como listener
        equipos.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        String nombreEquipo;

        nombreEquipo= ((TextView)view).getText().toString();

        Intent objetivo;
        objetivo= new Intent(getBaseContext(), Escudos.class);

        //renombrar
        objetivo.putExtra("nombre",nombreEquipo);

        startActivity(objetivo);

    }

    @Override
    public void onClick(View view) {

        MaterialButton botonLista=(MaterialButton) view;
        String textoLista;
        textoLista= botonLista.getText().toString();

        if (textoLista.equals("ATRAS")){
            Intent intLista = new Intent(this, ActivityRes.class);
            startActivity(intLista);
        }
    }
}