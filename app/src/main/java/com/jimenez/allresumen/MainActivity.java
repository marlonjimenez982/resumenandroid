package com.jimenez.allresumen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    MaterialButton btnIniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIniciar=findViewById(R.id.btnIniciar);

        btnIniciar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        MaterialButton botonIniciar=(MaterialButton) view;
        String textoIniciar;
        textoIniciar= botonIniciar.getText().toString();

        if (textoIniciar.equals("Iniciar")) {
            Intent i = new Intent(this, ActivityRes.class);
            startActivity(i);

        }
    }
}