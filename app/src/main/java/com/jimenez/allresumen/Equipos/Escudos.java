package com.jimenez.allresumen.Equipos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.jimenez.allresumen.ActivityListaEquipos;
import com.jimenez.allresumen.ActivityRes;
import com.jimenez.allresumen.R;

public class Escudos extends AppCompatActivity implements View.OnClickListener{

    ImageView escudo;

    MaterialButton btnAtrasEsc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escudos);

        //Boton Atras
        btnAtrasEsc=findViewById(R.id.btnAtrasEsc);
        btnAtrasEsc.setOnClickListener(this);

        escudo=findViewById(R.id.escudoequipo);

        //capturar Intent
        Intent myIntent=getIntent();
        //Obtener el nombre del equipo
        String dato=myIntent.getStringExtra("nombre");

        Toast.makeText(getBaseContext(), dato, Toast.LENGTH_LONG).show(); //PArte1

        dato=dato.replaceAll(" ","_"); //Reemplazar
        dato=dato.toLowerCase(); //Mayuscula a minuscula
        dato="escudo_de_"+dato;

        //Buscar el recurso de acuerdo al texto anterior
        Resources res= getResources();
        int id_res= res.getIdentifier(dato , "drawable", getPackageName());

        //cargar el id_res
        escudo.setImageResource(id_res);

    }

    @Override
    public void onClick(View view) {

        MaterialButton botonEscudo=(MaterialButton) view;
        String textoEscudo;
        textoEscudo= botonEscudo.getText().toString();

        if (textoEscudo.equals("ATRAS")){
            Intent intLisEsq = new Intent(this, ActivityListaEquipos.class);
            startActivity(intLisEsq);
        }
    }
}